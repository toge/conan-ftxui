from conans import ConanFile, CMake, tools
import os
import glob

class FtxuiConan(ConanFile):
    name = "ftxui"
    license = "MIT"
    url = "https://bitbucket.org/toge/conan-ftxui/"
    homepage = "https://github.com/ArthurSonzogni/FTXUI/"
    description = "C++ Functional Terminal User Interface."
    topics = ("ui", "terminal", "tui")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    @property
    def _source_subfolder(self):
        return os.path.join(self.source_folder, "source_subfolder")

    def _cmake(self):
        cmake = CMake(self)
        cmake.definitions["FTXUI_BUILD_EXAMPLES"] = False
        cmake.definitions["FTXUI_BUILD_DOCS"] = False
        cmake.configure(source_folder=self._source_subfolder)
        return cmake

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        extracted_dir = glob.glob('FTXUI-*/')[0]
        os.rename(extracted_dir, self._source_subfolder)

        tools.replace_in_file(
            self._source_subfolder + "/cmake/ftxui_set_options.cmake", 
            "set_target_properties(${library} PROPERTIES PREFIX \"ftxui-\")",
            "set_target_properties(${library} PROPERTIES PREFIX \"libftxui-\")")

    def build(self):
        cmake = self._cmake()
        cmake.build()

    def package(self):
        self.copy("LICENSE", dst="licenses", src=self._source_subfolder)
        cmake = self._cmake()
        cmake.install()
        tools.remove_files_by_mask(os.path.join(self.package_folder, "lib"), "*.cmake")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        if self.settings.os == "Linux":
            self.cpp_info.system_libs.append("pthread")
